# DIY stack #

Below are listed the list of micro libraries that form the 'DIY' stack used in [APIs with Ruby](http://book.apiswithruby.io)



* [Grape](http://intridea.github.io/grape/) - An opinionated micro-framework for creating REST-like APIs in Ruby.
* [Ruby Object Mapper](http://rom-rb.org/) - Data mapping and persistence toolkit for Ruby